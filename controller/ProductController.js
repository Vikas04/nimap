const { Category } = require("../models/category");
const { Product } = require("../models/product");

exports.getProduct = async (req, res) => {

  const { page = 1, limit = 3 } = req.query
  const productList = await await Product.find().limit(limit * 1).skip((page - 1) * limit).populate("category");

  if (!productList) {
    return res.status(200).json({ data: "product not found" });
  }
  return res.status(200).json({ data: productList });
};


exports.postProduct = async (req, res) => {
  const category = await Category.findById(req.body.category);
  if (!category) return res.status(400).send("Invalid Category");

  let newProduct = await Product.create({
    name: req.body.name,
    brand: req.body.brand,
    category: req.body.category,
  });

  let productId = category.productId
  productId.push(newProduct.id)
  await Category.findByIdAndUpdate(category.id, { productId: productId })

  if (!newProduct) return res.status(500).json({ message: "The product cannot be created" })

  return res.status(200).json({ data: newProduct });

};

exports.postProductById = async (req, res) => {
  const product = await Product.findById(req.params.id).populate("category");

  if (!product) {
    return res.status(404).json({ data: "product not found" });
  }
  return res.status(200).json({ data: product });
};



exports.PutProduct = async (req, res) => {
  if (!mongoose.isValidObjectId(req.params.id)) {
    return res.status(400).send("Invalid Product Id");
  }
  const category = await Category.findById(req.body.category);
  if (!category) return res.status(400).json({data :"Invalid Category"});

  const product = await Product.findByIdAndUpdate(
    req.params.id,
    {
      name: req.body.name,
      brand: req.body.brand,
      category: req.body.category,
    },
    { new: true }
  );

  if (!product) return res.status(500).send("the product cannot be updated!");

  return res.status(200).json({ data: product });

};


exports.DeleteProduct = async(req, res) => {
  let product = await Product.findById(req.params.id)
  if (!product) {
    return res.status(404).json({ message: "product not found" })
  }
  let category
  if (product.category) {
    category = await Category.findById(product.category)
  }

  await Product.findByIdAndRemove(req.params.id)
  if (category) {
    let productId = category.productId
    productId = productId.filter(e => e != req.params.id);
    await Category.findByIdAndUpdate(category.id, { productId: productId })
  }
  return res.status(200).json({ message: "success" })

};




