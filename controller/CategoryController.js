const { Category } = require("../models/category")
const { Product } = require("../models/product")

exports.getCategory = async (req, res) => {
  const { page = 1, limit = 5 } = req.query
  
  const categoryList = await Category.find().limit(limit * 1).skip((page - 1) * limit).populate("productId")

  if (!categoryList) {
    return res.status(500).json({ message: "Something went wrong" });
  }
  return res.status(200).json({ data: categoryList });
};


exports.PostCategory = async (req, res) => {

  let newCategory = await Category.create({
    name: req.body.name,
    icon: req.body.icon,
    color: req.body.color,
  });

  if (!newCategory) return res.status(404).json({ message: "the category cannot be created!" });

  return res.status(200).json({ data: newCategory });
};

exports.getCategoryById = async (req, res) => {
  const category = await Category.findById(req.params.id);

  if (!category) {
    return res
      .status(500)
      .json({ message: "the category with the given ID not found" });
  }
  return res.status(200).json({ data: category });
};

exports.putCategoryById = async (req, res) => {
  const category = await Category.findByIdAndUpdate(
    req.params.id,
    {
      name: req.body.name,
      icon: req.body.icon,
      color: req.body.color,
    },
    { new: true }
  );

  if (!category) return res.status(400).json({ message: "the category cannot be created" });

  return res.status(200).json({ data: category });
};


exports.DeleteCategoryById = async (req, res) => {

  let category = await Category.findById(req.params.id)
  if (!category) {
    return res.status(404).json({ message: "category not found" })
  }
  await Category.findByIdAndRemove(req.params.id)

  await Product.remove({ category: req.params.id })

  return res.status(200).json({ message: "success" })

};
