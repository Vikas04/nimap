const mongoose = require("mongoose");

const categorySchema = mongoose.Schema({
  name: { type: String, required: true },
  icon: { type: String, },
  color: { type: String },
  productId: [
    {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Product'
    }
],
dateCreated: { type: Date, default: Date.now },
});

exports.Category = mongoose.model("Category", categorySchema);
