const mongoose = require("mongoose");

const productSchema = mongoose.Schema({
  name: { type: String, required: true },
  brand: { type: String, default: "" },
  category:[
     {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Category",
    required: true,
  }
],
  dateCreated: { type: Date, default: Date.now },
});

exports.Product = mongoose.model("Product", productSchema);

