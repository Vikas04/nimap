const { Product } = require("../models/product");
const express = require("express");
const router = express.Router();
const { Category } = require("../models/category");
const mongoose = require("mongoose");
const ProductController = require('../controller/ProductController')



router
  .route('/')
  .get( ProductController.getProduct)
  .post(ProductController.postProduct);


  router
  .route('/:id')
  .get(ProductController.postProductById)
  .put(ProductController.PutProduct)
  .delete(ProductController.DeleteProduct);



module.exports = router;
