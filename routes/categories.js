const { Category } = require("../models/category");
const express = require("express");
const router = express.Router();
const CategoryController = require('../controller/CategoryController')


router
  .route('/')
  .get( CategoryController.getCategory)
  .post(CategoryController.PostCategory)
  

  router
  .route('/:id')
  .get(CategoryController.getCategoryById)
  .put(CategoryController.putCategoryById)
  .delete(CategoryController.DeleteCategoryById);

module.exports = router;
